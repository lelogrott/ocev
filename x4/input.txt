//MODO DE ENTRADA 1 = TRUE E 0 = FALSE
//ENTRAR NORMALMENTE COM NUMEROS 
// PONTO FLUTUANTE USANDO PONTO (".") E NÃO VIRGULA (",")

QTD DE EXECUCOES   = 10
QTD DE GERACOES    = 5000
QTD DE INDIVIDUOS  = 20

//tamanho do cromossomo pode ser o numero de bits se for uma
//solucao binaria ou pode ser o numero de variaveis se for uma
//solucao real

SOLUCAO BINARIA    = 0
SOLUCAO REAL       = 1
TAMANHO CROMOSSOMO = 10 
LIMITE INFERIOR    = -32
LIMITE SUPERIOR    = 32

ELITISMO           = 1

//tipos de crossover
//1-crossover normal (1 ponto de troca)
//2-crossover uniforme (multiplos pontos de troca)
//3-crossover aritmetico
//4-crossover blx
//chance do crossover eh valor entre 0 e 1

CROSSOVER          = 4
CHANCE CROSSOVER  = 0.85

//tipos de mutacao
//1-binaria
//2-real

MUTACAO            = 2
CHANCE MUTACAO     = 0.1

//tipos de selecao
//1-roleta
//2-torneio

SELECAO            = 1
TAMANHO TORNEIO    = 2

//metodos para otimizacao da diversidade
// 1 para true e 0 para false

GENERATION GAP     = 0
ESCALONAMENTO LINEAR = 1

TAMANHO DO GAP     = 0.6
FITNESS SHARING    = 0

CROWDING           = 0

// funcao de fitness
//  schaffer          = 0
//  algebrica_real    = 1
//  bits_alternados   = 2
//  algebrica_binario = 3
//  radios_binario    = 4
//  F3N               = 5
//  F3SN              = 6
//  DN_4              = 7
// rastrigin          = 8
// ackley             = 9

FUNCAO FITNESS     = 9
