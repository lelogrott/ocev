from copy import deepcopy
import re

def read_file():
    p = re.compile('\d+\.\d+|-\d+\.\d+|-\d+|\d+')
    fp = open("input.txt", "r")
    leitura = fp.read()
    linhas = leitura.splitlines()
    decisao = []
    aux=[]
    for i in range(0,len(linhas)):
        if(not linhas[i].startswith("//")):
            aux.append(linhas[i])
    linhas = deepcopy(aux)
    [[decisao.append(float(s)) for s in p.findall(i)] for i in linhas]
    fp.close()
    return decisao;
##########################################
# VETOR DECISAO
# 0 = qtd de execucoes
# 1 = qtd de geracoes
# 2 = qtd de individuos
# 3 = solucao binaria
# 4 = solucao real
# 5 = tamanho cromossomo
# 6 = limite inferior
# 7 = limite superior
# 8 = elitismo
# 9 = tipo crossover
# 10 = chance crossover
# 11 = mutacao
# 12 = chance mutacao
# 13 = tipo selecao
# 14 = tamanho torneio
# 15 = generation gap
# 16 = escalonamento linear
# 17 = tamanho do gap
# 18 = fitness sharing
# 19 = crowding
# 20 = funcao de fitness
###########################################
