import math as mt
import numpy as np
from copy import deepcopy

def schaffer(guy):
	func=sum(((guy.cromossomo[i]**2 + guy.cromossomo[i+1]**2)**0.25)*(mt.sin(50*(guy.cromossomo[i]**2 + guy.cromossomo[i+1]**2)**0.1)**2 + 1) for i in range(0,len(guy.cromossomo)-1))
	guy.fitness= 50.0 - func
	if(guy.fitness<0):
		guy.fitness=0
	return guy.fitness;

#calculo para funcao algebrica

def algebrica_real(guy):
	x = float(guy.cromossomo[0])
	y=mt.cos(20.0*x) - (abs(x)/2.0) + x**3.0/4.0 +4
	guy.fitness=y
	return y;

def bits_alternados(guy): 
    prev_bit=guy.cromossomo[0]
    guy.fitness = 0
    for i in range(1,len(guy.cromossomo)):
        if(prev_bit != guy.cromossomo[i]):
            guy.fitness += 1
        prev_bit=guy.cromossomo[i]
    return guy.fitness;  

def algebrica_binario(guy):
	str1 = ''.join(str(i) for i in guy.cromossomo)
	x = -2.0 +float(4.0/(2.0**16.0-1))*int(str1,2)
	y=mt.cos(20*x) - (abs(x)/2) + x**3/4 +4
	guy.fitness=y
	return y;

#calculo para funcao dos radios 5 bits por gene

def radios_binario(guy):
	str1 = ''.join(str(i) for i in guy.cromossomo[:5])
	str2 = ''.join(str(i) for i in guy.cromossomo[5:])
	xST = 0 + float(24.0/(2.0**5.0-1))*int(str1,2)
	xLX = 0 + float(16.0/(2.0**5.0-1))*int(str2,2)
	xST = mt.ceil(xST)
	xLX = mt.ceil(xLX)
	
	if(int(str1,2) + 2*int(str2,2) > 40):
		guy.fitness = (30*xST + 40*xLX)/1360.0 - 2*max(0, (xST + 2*xLX-40)/16)
	else:
		guy.fitness = (30*xST + 40*xLX)/1360.0
	guy.fitness *= 1360
	if(guy.fitness < 0):
		guy.fitness = 0
	return guy.fitness;	

#deceptive functions

def F3N(guy):
	tripla=[]
	guy.fitness=0
	for i in guy.cromossomo:
		tripla.append(i)
		if (len(tripla)==3):
			if(tripla == [0,0,0]):
				guy.fitness+= 28
			if(tripla == [0,0,1]):
				guy.fitness+= 26
			if(tripla == [0,1,0]):
				guy.fitness+= 22
			if(tripla == [0,1,1]):
				guy.fitness+= 0
			if(tripla == [1,0,0]):
				guy.fitness+= 14
			if(tripla == [1,0,1]):
				guy.fitness+= 0
			if(tripla == [1,1,0]):
				guy.fitness+= 0
			if(tripla == [1,1,1]):
				guy.fitness+= 30
			tripla=[]
	return guy.fitness;

def F3SN(guy):
	guy.fitness=0
	for i in range(len(guy.cromossomo)-20):
		tripla = [guy.cromossomo[i], guy.cromossomo[i+10], guy.cromossomo[i+20]]
		if(tripla == [0,0,0]):
			guy.fitness+= 28
			if(tripla == [0,0,1]):
				guy.fitness+= 26
			if(tripla == [0,1,0]):
				guy.fitness+= 22
			if(tripla == [0,1,1]):
				guy.fitness+= 0
			if(tripla == [1,0,0]):
				guy.fitness+= 14
			if(tripla == [1,0,1]):
				guy.fitness+= 0
			if(tripla == [1,1,0]):
				guy.fitness+= 0
			if(tripla == [1,1,1]):
				guy.fitness+= 30
	return guy.fitness

def DN_4(guy):
	quadrupla = []
	guy.fitness = 0
	for i in guy.cromossomo:
		quadrupla.append(i)
		if(len(quadrupla)==4):
			if(quadrupla.count(1)>0):
				guy.fitness+=quadrupla.count(1)
			else:
				guy.fitness+=len(guy.cromossomo)+1
			quadrupla = []
	return guy.fitness;
	
def rastrigin(guy):
	guy.fitness = 10*len(guy.cromossomo)
	for i in guy.cromossomo:
		guy.fitness += i**2 - (10*mt.cos(2*mt.pi*i))
	guy.fitness = 150 - guy.fitness
	return guy.fitness;

def ackley(guy):
	firstSum = 0.0
	secondSum = 0.0
	for c in guy.cromossomo:
		firstSum += c**2.0
		secondSum += mt.cos(2.0*mt.pi*c)
	n = float(len(guy.cromossomo))
	guy.fitness = 50-(-20.0*mt.exp(-0.2*mt.sqrt(firstSum/n)) - mt.exp(secondSum/n) + 20 + mt.e)
	return guy.fitness;
