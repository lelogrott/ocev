from read import *
from functions import *
import random
import math as mt
import numpy as np
import sys
from copy import deepcopy
import time

##########################################
# VETOR DECISAO
# 0 = qtd de execucoes
# 1 = qtd de geracoes
# 2 = qtd de individuos
# 3 = solucao binaria
# 4 = solucao real
# 5 = tamanho cromossomo
# 6 = limite inferior
# 7 = limite superior
# 8 = elitismo
# 9 = tipo crossover
# 10 = chance crossover
# 11 = mutacao
# 12 = chance mutacao
# 13 = tipo selecao
# 14 = tamanho torneio
###########################################

class Individuo:
    def __init__(self, cromossomo):
        self.cromossomo = cromossomo
        self.fitness=-10
        
## inicio variaveis globais ##
vetor_decisao = read_file()
POP_ARRAY=[]
FITNESS_ABSOLUTO=0
MELHOR_INDIVIDUO = Individuo([random.uniform(-20,20) for j in range(0, int(vetor_decisao[5]))])
FUNCTION_LIST = [schaffer, algebrica_real, bits_alternados, algebrica_binario, radios_binario, F3N, F3SN, DN_4, rastrigin, ackley]

################################### 
## HEAP SORT FUNCTIONS - ROSETTA ##
###################################
def heapsort(lst):
   
  # in pseudo-code, heapify only called once, so inline it here
  for start in range((len(lst)-2)/2, -1, -1):
    siftdown(lst, start, len(lst)-1)
 
  for end in range(len(lst)-1, 0, -1):
    lst[end], lst[0] = lst[0], lst[end]
    siftdown(lst, 0, end - 1)
  return lst;
 
def siftdown(lst, start, end):
  root = start
  while True:
    child = root * 2 + 1
    if child > end: break
    if child + 1 <= end and lst[child].fitness < lst[child + 1].fitness:
      child += 1
    if lst[root].fitness < lst[child].fitness:
      lst[root], lst[child] = lst[child], lst[root]
      root = child
    else:
      break
###################################
## HEAP SORT FUNCTIONS - ROSETTA ##
###################################


def init_pop(n_people):
    for i in range(0,n_people):
    	if(vetor_decisao[4]):
	        guy = Individuo([random.uniform(vetor_decisao[6],vetor_decisao[7]) for j in range(0, int(vetor_decisao[5]))])
        elif(vetor_decisao[3]):
        	guy = Individuo([random.randint(0,1) for j in range(0, int(vetor_decisao[5]))])
        POP_ARRAY.append(guy)        
    return;
    
def show_pop(cur_pop):
    for i in range(0,len(cur_pop)):
        print(cur_pop[i].cromossomo)
        print(cur_pop[i].fitness)
    return;

def atualiza_fitness(cur_pop):
	global FITNESS_ABSOLUTO
	global MELHOR_INDIVIDUO
	FITNESS_ABSOLUTO = 0
	MELHOR_INDIVIDUO=deepcopy(cur_pop[0])
	for i in range(0,len(cur_pop)):
		fitness = FUNCTION_LIST[int(vetor_decisao[20])](cur_pop[i])
		if(fitness > MELHOR_INDIVIDUO.fitness):
			MELHOR_INDIVIDUO = deepcopy(cur_pop[i])
		FITNESS_ABSOLUTO += fitness
	return;
	
def roleta(cur_pop):
	casal=[]
	flag=0
	while(len(casal)<2):
		agulha = random.uniform(0,1)
		for individuo in cur_pop:
			if(agulha < float(individuo.fitness)/float(FITNESS_ABSOLUTO)):
				if((flag==1 and casal[0]!=individuo) or (flag==0)):
					casal.append(individuo)
					flag=1
					break
			agulha = agulha - float(individuo.fitness)/float(FITNESS_ABSOLUTO)
	return deepcopy(casal)

def torneio(cur_pop):
	casal=[]
	sorteados=[]
	first = 1
	while(len(casal)<2):
		sorteados = random.sample(range(0,len(cur_pop)),int(vetor_decisao[14]))
		for i in range(0,len(sorteados)):
			sorteados[i] = cur_pop[sorteados[i]]
		sorteados = heapsort(sorteados)
		casal.append(sorteados[len(sorteados)-1])
		cur_pop.pop(cur_pop.index(sorteados[len(sorteados)-1]))
	return deepcopy(casal)
	
def crossover(casal):
	if(random.uniform(0.0,1.0) < vetor_decisao[10]):
		pos = random.randint(0,vetor_decisao[5])
		cromo_aux = casal[0].cromossomo[:pos]+casal[1].cromossomo[pos:]
		casal[1].cromossomo = casal[1].cromossomo[:pos]+casal[0].cromossomo[pos:]
		casal[0].cromossomo = cromo_aux
		return casal;
	return 0;
	
def uniform_crossover(casal):
	if(random.uniform(0.0,1.0) < vetor_decisao[10]):
		for i in range(0,int(vetor_decisao[5])):
			if(random.randint(0,1)):
				aux = casal[0].cromossomo[i]
				casal[0].cromossomo[i] = casal[1].cromossomo[i]
				casal[1].cromossomo[i] = aux
		return casal;	
	return 0;

def arithmetic_crossover(casal):
	if(random.uniform(0.0,1.0) < vetor_decisao[10]):
		a = random.uniform(0,1)
		cromo_aux = []
		cromo_aux2 = []
		for i in range(len(casal[0].cromossomo)):
			cromo_aux.append(a * casal[0].cromossomo[i] + (1-a) * casal[1].cromossomo[i])
			cromo_aux2.append((1-a) * casal[0].cromossomo[i] + a * casal[1].cromossomo[i])
		casal[0].cromossomo = deepcopy(cromo_aux)
		casal[1].cromossomo = deepcopy(cromo_aux2)		
		return casal;
	return 0;

def BLX_crossover(casal):
	if(random.uniform(0.0,1.0) < vetor_decisao[10]):
		a = 0.5
		cromo_aux = []
		cromo_aux2 = []
		
		for i in range(len(casal[0].cromossomo)):
			d = abs(casal[0].cromossomo[i] - casal[1].cromossomo[i])
			u = random.uniform(min(casal[0].cromossomo[i],casal[1].cromossomo[i])-a*d, max(casal[0].cromossomo[i], casal[1].cromossomo[i])+a*d)
			cromo_aux.append(u)
			u = random.uniform(min(casal[0].cromossomo[i],casal[1].cromossomo[i])-a*d, max(casal[0].cromossomo[i], casal[1].cromossomo[i])+a*d)
			cromo_aux2.append(u)
		casal[0].cromossomo = deepcopy(cromo_aux)
		casal[1].cromossomo = deepcopy(cromo_aux2)
		for individuo in casal:
			for i in range(len(individuo.cromossomo)):
				if(individuo.cromossomo[i] < vetor_decisao[6]):
					individuo.cromossomo[i] = vetor_decisao[6]
				if(individuo.cromossomo[i] > vetor_decisao[7]):
					individuo.cromossomo[i] = vetor_decisao[7]		
		return casal;
	return 0;

def mutate_pop(cur_pop):
	for individuo in cur_pop:
		for i in range(0,len(individuo.cromossomo)):
			if(random.uniform(0.0,1.0) < vetor_decisao[12]):
					individuo.cromossomo[i] += random.uniform(-1.0,1.0)
					if(individuo.cromossomo[i] < vetor_decisao[6]):
						individuo.cromossomo[i] = vetor_decisao[6]
					if(individuo.cromossomo[i] > vetor_decisao[7]):
						individuo.cromossomo[i] = vetor_decisao[7]
	return;
	
def mutate_binpop(cur_pop):
	for individuo in cur_pop:
		for i in range(0,len(individuo.cromossomo)):
			if(random.uniform(0.0,1.0) < vetor_decisao[12]):
				if (individuo.cromossomo[i]==1):
					individuo.cromossomo[i]=0
				else:
					individuo.cromossomo[i]=1
	return;

def hamming(ind1, ind2):
	assert len(ind1.cromossomo) == len(ind2.cromossomo)	
	return sum(ch1 != ch2 for ch1, ch2 in zip(ind1.cromossomo,ind2.cromossomo))

def media_hamming(populacao):
	media = 0.0
	aux = 0
	for i in range(len(populacao)):
		for j in range(i, len(populacao)):
			media += hamming(populacao[i], populacao[j])
			aux+=1
	return media/float(aux)

def euclides(ind1, ind2):
	euclidiana=0.0
	for i in range(len(ind1.cromossomo)):
		euclidiana += (ind1.cromossomo[i] - ind2.cromossomo[i])**2
	return np.sqrt(euclidiana)

def distancia_euclidiana(populacao):
	media = 0.0
	aux = 0
	for i in range(len(populacao)):
		for j in range(i, len(populacao)):
			media += euclides(populacao[i], populacao[j])
			aux+=1
	return media/float(aux)

def escalonamento_linear(cur_pop, c):
	fit_list = [k.fitness for k in cur_pop]
	fit_list = sorted(fit_list)
	fmin = fit_list[0]
	favg = np.mean(fit_list)
	fmax=fit_list[len(fit_list)-1]
	
	if(fmin > (c*favg-fmax)/(c-1)):
		alpha = (favg*(c-1)/(fmax-favg))
		beta = (favg*(fmax-c*favg)/(fmax-favg))
	else:
		alpha = favg/(favg-fmin)
		beta = -fmin*favg/(favg-fmin)
	
	for guy in cur_pop:
		guy.fitness = guy.fitness * alpha + beta
		if (guy.fitness < 0):
			guy.fitness = 0.001
	global FITNESS_ABSOLUTO
	FITNESS_ABSOLUTO = sum([k.fitness for k in cur_pop])

	#return cur_pop

def compartilha_fitness(cur_pop):
	for individuo in cur_pop:
		soma = 0.0
		for nigga in cur_pop:
			if(vetor_decisao[3]):
				soma += sharing(hamming(individuo, nigga))
			if(vetor_decisao[4]):
				soma += sharing(euclides(individuo, nigga))
		individuo.fitness /= soma

def sharing(distancia):
	alpha = 2
	sigma = 1
	if(distancia < sigma):
		return (1-(distancia/sigma)**alpha)
	else:
		return 0
	
def crowding_surf(pop_aux, cur_pop):
	CROWDING_SIZE = 5
	for bro in pop_aux:
		back_flip=[]
		haoles = random.sample(range(int(vetor_decisao[2])), CROWDING_SIZE)
		for j in range(len(haoles)):
			if(vetor_decisao[3]):
				back_flip.append(hamming(bro,cur_pop[haoles[j]]))
			if(vetor_decisao[4]):
				back_flip.append(euclides(bro,cur_pop[haoles[j]]))
		for j in range(len(back_flip)):
			if(back_flip[j] == min(back_flip)):
				cur_pop[haoles[j]]=bro
				break
	return cur_pop

if(vetor_decisao[4]):
	f=open('out.txt','w')
	f2=open('out2.txt','w')
	f3=open('out3.txt','w')
	to_plot=[0]*int(vetor_decisao[1])
	variancia=[0]*int(vetor_decisao[1])
	media_da_geracao = [0] * int(vetor_decisao[1])
	qtd_execucoes=0
	melhor_alcancado = -10
	melhores_individuos_ever=[]
	total_geracoes=0
	
	
	
	aux_inicio = 0.3*vetor_decisao[1] 
	aux_final = 0.8*vetor_decisao[1]
	aux_escalonamento = 0.8/(aux_final-aux_inicio)
	#print(aux_inicio, aux_final, aux_escalonamento)
	
	
	while(qtd_execucoes < int(vetor_decisao[0])):
		qtd_geracoes=0
		#print "execucao numero %d"%(qtd_execucoes)
		POP_ARRAY = []
		c = 1.2
		init_pop(int(vetor_decisao[2]))
		cont = 0.1
		while(qtd_geracoes < int(vetor_decisao[1])):
			total_geracoes+=1
			pop_aux=[]
			if(porcentagem==0):
				print"[ _ _ _ _ _ _ _ _ _ _ ] [0%]\r",
				sys.stdout.flush()
			atualiza_fitness(POP_ARRAY)
			#show_pop(POP_ARRAY)
			#time.sleep(10)
			if((total_geracoes%(0.1*vetor_decisao[1]*vetor_decisao[0]))==0):
				porcentagem += 10
				print "[",
				for i in range(porcentagem/10):
					print "=",
				print ">",
				for i in range(10-(porcentagem/10)):
					print "_",
				print "] [%d %%]\r"%porcentagem,
				sys.stdout.flush()
			if(MELHOR_INDIVIDUO.fitness > melhor_alcancado):
				melhor_alcancado = MELHOR_INDIVIDUO.fitness
			to_plot[qtd_geracoes] += MELHOR_INDIVIDUO.fitness
			media_da_geracao[qtd_geracoes] += np.mean([k.fitness for k in POP_ARRAY])
			
			
			variancia[qtd_geracoes]+=distancia_euclidiana(POP_ARRAY)
			if(vetor_decisao[16]):
				if(qtd_geracoes < aux_inicio):
					escalonamento_linear(POP_ARRAY,1.2)
				elif(qtd_geracoes > aux_final):				
					escalonamento_linear(POP_ARRAY,2.0)
				else:
					c += 0.8/(aux_final - aux_inicio)
					escalonamento_linear(POP_ARRAY,c)
			
			if(vetor_decisao[18]):
				compartilha_fitness(POP_ARRAY)
			
			if(vetor_decisao[15]):
				nivel_fornicacao = int(vetor_decisao[17]*vetor_decisao[2])/2
				if(qtd_geracoes>0.8*vetor_decisao[1]):
					nivel_fornicacao=int(vetor_decisao[2])/2
			else:
				nivel_fornicacao = int(vetor_decisao[2])/2
			'''
			metodo para escada de evolucao. a cada 10 porcento aumenta o gap
			if(vetor_decisao[15]):
				if((qtd_geracoes%(0.1*vetor_decisao[1]))==0):
					cont+=0.1
				nivel_fornicacao=int(cont*vetor_decisao[2])/2
			else:
				nivel_fornicacao = int(vetor_decisao[2])/2
			'''
			for pares in range(0,nivel_fornicacao):
				if(vetor_decisao[13]==1):
				    selecionados = roleta(deepcopy(POP_ARRAY))
				elif(vetor_decisao[13]==2):
				    selecionados = torneio(deepcopy(POP_ARRAY))
				if(vetor_decisao[9]==1):
				    casal = crossover(selecionados)
				elif(vetor_decisao[9]==2):
				    casal = uniform_crossover(selecionados)
				elif(vetor_decisao[9]==3):
					casal = arithmetic_crossover(selecionados)
				elif(vetor_decisao[9]==4):
					casal = BLX_crossover(selecionados)
				if(casal==0):
				    pop_aux.append(selecionados[0])
				    pop_aux.append(selecionados[1])
				else:
				    pop_aux.append(casal[0])
				    pop_aux.append(casal[1])
			mutate_pop(pop_aux)
			if(vetor_decisao[15]):
				pop_aux = pop_aux + POP_ARRAY[nivel_fornicacao*2::1]
			if(vetor_decisao[19]):
			    POP_ARRAY = deepcopy(crowding_surf(deepcopy(pop_aux), deepcopy(POP_ARRAY)))
			else:
			    POP_ARRAY = deepcopy(pop_aux)
			if(vetor_decisao[8]):	
				POP_ARRAY[0] = deepcopy(MELHOR_INDIVIDUO)
			qtd_geracoes += 1
		qtd_execucoes +=1
		melhores_individuos_ever.append(deepcopy(MELHOR_INDIVIDUO))

	media_geracoes=[]
	for i in range(len(to_plot)):
		media_geracoes.append(float(to_plot[i])/vetor_decisao[0]) 
		print >> f, "%f"%(media_geracoes[i])
	for i in media_da_geracao:
		print >> f2, "%f"%(float(i)/vetor_decisao[0])
	for i in variancia:
		print >> f3, "%f"%(float(i)/vetor_decisao[0])

	#calculo de media, desvio padrao e variancia dos melhores individuos de cada execucao
	media = np.mean([k.fitness for k in melhores_individuos_ever])
	desvio_padrao = np.std([k.fitness for k in melhores_individuos_ever])
	variancia = np.var([k.fitness for k in melhores_individuos_ever])
	print "\n\n\n"
	print "---------------------------"
	print "      FIM DA EXECUCAO"
	print "---------------------------"
	print "%d execucoes realizadas\n%d geracoes por execucao"%(int(vetor_decisao[0]),int(vetor_decisao[1]))
	print "melhor individuo calculado = %f"%(melhor_alcancado)
	print "media de %d execucoes = %f"%(vetor_decisao[0], media)
	print "desvio padrao = %f"%(desvio_padrao)
	print "variancia = %f "%(variancia)
	
if(vetor_decisao[3]):
	f=open('out.txt','w')
	f2=open('out2.txt','w')
	f3=open('out3.txt','w')
	to_plot=[0]*int(vetor_decisao[1])
	variancia=[0]*int(vetor_decisao[1])
	media_da_geracao = [0] * int(vetor_decisao[1])
	qtd_execucoes=0
	melhor_alcancado = -10
	melhores_individuos_ever=[]
	aux_inicio = 0.3*vetor_decisao[1] 
	aux_final = 0.8*vetor_decisao[1]
	aux_escalonamento = 0.8/(aux_final-aux_inicio)
	c = 1.2
	total_geracoes=0
	porcentagem = 0
	while(qtd_execucoes < int(vetor_decisao[0])):
		qtd_geracoes=0
		POP_ARRAY = []
		init_pop(int(vetor_decisao[2]))
		#print "execucao numero %d"%(qtd_execucoes)
		while(qtd_geracoes < int(vetor_decisao[1])):
			total_geracoes+=1
			pop_aux=[]
			if(porcentagem==0):
				print"[ _ _ _ _ _ _ _ _ _ _ ] [0%]\r",
				sys.stdout.flush()
			atualiza_fitness(POP_ARRAY)
			#show_pop(POP_ARRAY)
			#time.sleep(10)
			if((total_geracoes%(0.1*vetor_decisao[1]*vetor_decisao[0]))==0):
				porcentagem += 10
				print "[",
				for i in range(porcentagem/10):
					print "=",
				print ">",
				for i in range(10-(porcentagem/10)):
					print "_",
				print "] [%d %%]\r"%porcentagem,
				sys.stdout.flush()
			if(MELHOR_INDIVIDUO.fitness > melhor_alcancado):
				melhor_alcancado = MELHOR_INDIVIDUO.fitness
				melhor_ind_alcancado = deepcopy(MELHOR_INDIVIDUO)
			to_plot[qtd_geracoes] += MELHOR_INDIVIDUO.fitness
			media_da_geracao[qtd_geracoes] += np.mean([k.fitness for k in POP_ARRAY])
			variancia[qtd_geracoes]+=media_hamming(POP_ARRAY)
			if(vetor_decisao[16]):
				if(qtd_geracoes < aux_inicio):
					escalonamento_linear(POP_ARRAY,1.2)
				elif(qtd_geracoes > aux_final):				
					escalonamento_linear(POP_ARRAY,2.0)
				else:
					c += 0.8/(aux_final - aux_inicio)
					escalonamento_linear(POP_ARRAY,c)
			if(vetor_decisao[18]):
				compartilha_fitness(POP_ARRAY)
			if(vetor_decisao[15]):
				nivel_fornicacao = int(vetor_decisao[17]*vetor_decisao[2])/2
				if(qtd_geracoes>0.8*vetor_decisao[1]):
					nivel_fornicacao=int(vetor_decisao[2])/2
			else:
				nivel_fornicacao = int(vetor_decisao[2])/2
			'''
			metodo para escada de evolucao. a cada 10 porcento aumenta o gap
			if(vetor_decisao[15]):
				if((qtd_geracoes%(0.1*vetor_decisao[1]))==0):
					cont+=0.1
				nivel_fornicacao=int(cont*vetor_decisao[2])/2
			else:
				nivel_fornicacao = int(vetor_decisao[2])/2
			'''
			for pares in range(0,nivel_fornicacao):
				if(vetor_decisao[13]==1):
				    selecionados = roleta(deepcopy(POP_ARRAY))
				elif(vetor_decisao[13]==2):
				    selecionados = torneio(deepcopy(POP_ARRAY))
				if(vetor_decisao[9]==1):
				    casal = crossover(selecionados)
				elif(vetor_decisao[9]==2):
				    casal = uniform_crossover(selecionados)
				if(casal==0):
				    pop_aux.append(selecionados[0])
				    pop_aux.append(selecionados[1])
				else:
				    pop_aux.append(casal[0])
				    pop_aux.append(casal[1])
			mutate_binpop(pop_aux)
			
			if(vetor_decisao[19]):
			    POP_ARRAY = deepcopy(crowding_surf(deepcopy(pop_aux), deepcopy(POP_ARRAY)))
			else:
			    POP_ARRAY = deepcopy(pop_aux)
			
			if(vetor_decisao[8]):	
				POP_ARRAY[0] = deepcopy(MELHOR_INDIVIDUO)
			qtd_geracoes += 1
		qtd_execucoes +=1
		melhores_individuos_ever.append(deepcopy(MELHOR_INDIVIDUO))

	media_geracoes=[]
	for i in range(len(to_plot)):
		media_geracoes.append(float(to_plot[i])/vetor_decisao[0]) 
		print >> f, "%f"%(media_geracoes[i])
	for i in media_da_geracao:
		print >> f2, "%f"%(float(i)/vetor_decisao[0])
	for i in variancia:
		print >> f3, "%f"%(float(i)/vetor_decisao[0])
	#calculo de media, desvio padrao e variancia dos melhores individuos de cada execucao
	media = np.mean([k.fitness for k in melhores_individuos_ever])
	desvio_padrao = np.std([k.fitness for k in melhores_individuos_ever])
	variancia = np.var([k.fitness for k in melhores_individuos_ever])

	print "\n\n\n"
	print "---------------------------"
	print "      FIM DA EXECUCAO"
	print "---------------------------"
	print "%d execucoes realizadas\n%d geracoes por execucao"%(int(vetor_decisao[0]),int(vetor_decisao[1]))
	print "fitness do melhor individuo calculado = %f"%(melhor_alcancado)
	print "melhor individuo calculado = "
	aux=[]
	aux.append(melhor_ind_alcancado)
	show_pop(aux)
	print "media de %d execucoes = %f"%(vetor_decisao[0], media)
	print "desvio padrao = %f"%(desvio_padrao)
	print "variancia = %f "%(variancia)

    
